package com.expedia.bcd.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories
@SpringBootApplication
public class Trends{

    public static void main(String[] args) {
        SpringApplication.run(Trends.class, args);
	}

}
