package com.expedia.bcd.demo;

import org.springframework.data.annotation.Id;

public class Place {

    @Id
    private String id;
    private String location;
    private Integer bookingsCount;
    private String platform;

    Place(String id, String location, Integer bookingsCount, String platform) {
        this.id = id;
        this.location = location;
        this.bookingsCount = bookingsCount;
        this.platform = platform;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getBookingsCount() {
        return bookingsCount;
    }

    public void setBookingsCount(Integer bookingsCount) {
        this.bookingsCount = bookingsCount;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    @Override
    public String toString() {
        return "Place{" +
                "id='" + id + '\'' +
                ", location='" + location + '\'' +
                ", bookingsCount=" + bookingsCount +
                ", platform='" + platform + '\'' +
                '}';
    }
}
